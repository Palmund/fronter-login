// Save this script as `options.js`

// Saves options to localStorage.
function save_options() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    localStorage["username"] = username;
    localStorage["password"] = password;

    // Update status to let user know options were saved.
    var status = document.getElementById("status");
    status.innerHTML = "Options Saved.";
    setTimeout(function() {
        status.innerHTML = "";
    }, 750);
}

// Restores select box state to saved value from localStorage.
function restore_options() {
    var username = localStorage["username"];
    var password = localStorage["password"];
    document.getElementById('username').value = (username === undefined) ? '' : username;
    document.getElementById('password').value = (password === undefined) ? '' : password;
}

window.addEventListener('load', restore_options);
document.getElementById('save').addEventListener('click', save_options);