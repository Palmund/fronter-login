var urlPattern = new RegExp('.*fronter.com/.+', 'i');

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if (changeInfo.status === 'complete') {
        if (urlPattern.test(tab.url)) {
            console.log("URL: "+tab.url);
            var username = localStorage["username"];
            var password = localStorage["password"];
            if (username == undefined || password == undefined) {
                alert('Username or password has not been set correctly.');
            } else {
                chrome.tabs.executeScript(
                    tabId,
                    {
                    code:   "var usernameInput = document.getElementById('username_input');" +
                            "var passwordInput = document.getElementById('password_input');" +
                            "usernameInput.value = '"+username+"';" +
                            "passwordInput.value = '"+password+"';" +
                            "document.forms[0].submit();"
                    }
                );
            }
        }
    }
}
);